#include "KillerBunny.h"
#include "Archer.h"

void KillerBunny::move()
{
	cout << "Moving..." << endl;
}

void KillerBunny::attack(Archer* target)
{
	target->setHp(target->getHp() - mPower);
	cout << mName << " attacked " << target->getName() << ". " << mPower << " damage!" << endl;
}

void KillerBunny::crossCutter()
{
}

void KillerBunny::setName(string name)
{
	mName = name;
}

void KillerBunny::setPower(int power)
{
	mPower = power;
}

void KillerBunny::setHp(int value)
{
	mHp = value;
	if (mHp < 0) mHp = 0;
}

string KillerBunny::getName()
{
	return mName;
}

int KillerBunny::getHp()
{
	return mHp;
}

int KillerBunny::getPower()
{
	return mPower;
}
