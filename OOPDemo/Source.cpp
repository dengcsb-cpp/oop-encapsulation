#include <iostream>
#include <string>
#include "Archer.h"
#include "KillerBunny.h"

using namespace std;

int main()
{
	// Constructor 1
	Archer* archer = new Archer();
	archer->setName("Archer");
	archer->setHp(100);
	archer->setPower(10);

	// Constructor 2
	Archer* archer2 = new Archer("Archer 2");
	archer->setHp(100);
	archer->setPower(10);

	// Constructor 3
	Archer* archer3 = new Archer("Archer 3", 10, 100);

	// This is how to read data through getter
	cout << "HP: " << archer->getHp() << endl;

	// Killer Bunny
	KillerBunny* killerBunny = new KillerBunny();
	killerBunny->mName = "Killer Bunny";
	killerBunny->mHp = 120;
	killerBunny->mPower = 9;

	/* Attack the killer bunny
	Notice how you can just read the code out loud and still understand it*/
	archer->attack(killerBunny);
}
