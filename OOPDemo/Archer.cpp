#include "Archer.h"
// Include any forward-declared types in the header file here
#include "KillerBunny.h"

Archer::Archer()
{
	// Since there's no initial data provided, we'll provide our own default value.
	mName = "Default";
	mHp = 1;
	mPower = 1;
}

Archer::Archer(string name)
{
	// Copy over the provided data
	mName = name;

	// Set our own default value for the others
	mHp = 1;
	mPower = 1;
}

Archer::Archer(string name, int power, int hp)
{
	// Copy over provided data
	mName = name;
	mPower = power;
	mHp = mHp;
}

// 'Archer::' means the function is owned by the 'Archer' class
// This can be auto-generated from the header file
void Archer::move()
{
	cout << "Moving..." << endl;
}

void Archer::attack(KillerBunny* target)
{
	// Deal damage to the target through its HP setter
	// Read values through the getter
	// If you are accessing your own data, you don't need to go through the accessors
	target->setHp(target->getHp() - mPower);
	cout << mName << " attacked " << target->getName() << ". " << mPower << " damage!" << endl;
}

void Archer::strafe()
{
	cout << "Strafe used!" << endl;
}

void Archer::setName(string name)
{
	/* We simply copy over the data to our data member.
	This is actually the most common scenario */
	mName = name;
}

void Archer::setPower(int power)
{
	mPower = power;
}

void Archer::setHp(int value)
{
	// Inside this function, we have access to the private 'mHp' variable
	mHp = value;

	/* Filter the HP values for invalid values.
	Since this is the only access point to HP,
	any code is forced to go through this filter.*/
	if (mHp < 0) mHp = 0;
}

string Archer::getName()
{
	return mName;
}

int Archer::getHp()
{
	// We are now only providing a copy of the HP
	// It is common to simply return the value itself.
	return mHp;
}

int Archer::getPower()
{
	return mPower;
}

