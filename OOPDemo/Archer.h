#pragma once
#include <string>
#include <iostream>
using namespace std;

class KillerBunny;

class Archer
{
/* Everything under 'public' can be accessed anywhere*/
public:
	/* Custom constructors 
	The default constructor will be lost after writing 
	a single custom constructor.
	Its syntax is a function with no return (not void)
	and the function name should be the same as the class name*/
	Archer();
	Archer(string name);
	Archer(string name, int power, int hp);

	void move();
	void attack(KillerBunny* target);
	void strafe();

public:
	// Read values through the getter functions
	string getName();
	int getHp();
	int getPower();

	// Modify values through the setter functions
	void setName(string name);
	void setHp(int value);
	void setPower(int power);

/* Everything under 'private' is no longer accessible 
outside of this class */
private:
	string mName;
	int mHp;
	int mPower;
};
