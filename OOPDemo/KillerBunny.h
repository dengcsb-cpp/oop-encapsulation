#pragma once
#include <string>
using namespace std;

class Archer;

class KillerBunny
{
public:
	void move();
	void attack(Archer* target);
	void crossCutter();

public:
	string getName();
	int getHp();
	int getPower();

	void setName(string name);
	void setHp(int value);
	void setPower(int power);

private:
	string mName;
	int mHp;
	int mPower;
};

